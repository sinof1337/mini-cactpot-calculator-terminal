Created by Raymond Hubbard

"Mini Cactpot Calculator" is used to predict results in the "Mini Cactpot" lottery in Final Fantasy XIV. The user inputs digits for each possible location when prompted, with 1-9 being used for currently known values in the lottery, and 0 for unknown values. Other numbers, as well as letters and symbols, will not be accepted, and the user will be prompted for the for the location once again. After 4 known values are entered (the maximum amount in the minigame), the program returns the possible results for each "line" of digits, as well as the recommended selection for the user to make.

How to use:

At the top of the screen, there is a diagram of the in-game ticket. Numbers represent the final selection lines, and letters represent possible digit locations.

The program will prompt the user for an entry for each location, labeled by their assigned letter. Input the digit (or zero if unknown) and then press enter. After filling all locations, or if 4 known digits are entered, the program will calculate potential odds for each "line".

After calculating, the results will be displayed. Each line will have its maximum, minimum, and average rewards displayed, as well as the average reward rank (the best reward is ranked 1, second best is 2, etc). At the bottom, the best possible results for each catagory, and the line they are from, is displayed.

A final prompt is at the bottom. Input "R" to reset, or "E" to exit, then press enter.